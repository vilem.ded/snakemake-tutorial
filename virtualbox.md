# Setting up a virtual machine

## Install VirtualBox

Install the [Virtual Box platform package](https://www.virtualbox.org/wiki/Downloads) for your operating system and the matching [VM VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads).

## Import Ubuntu image

* Download `Ubuntu.ova` from the [WebDAV](https://webdav-r3lab.uni.lu/public/biocore/snakemake_tutorial/).
* In the VirtualBox menu select "File" -> "Import Appliance…" and select the `Ubuntu.ova` you downloaded previously.

## Configure shared directory

In the main windows of VirtualBox, select the Ubuntu VM and click on "Settings".

![vbox_settings](img/vbox_settings.png)

Then select the "Shared Folders" tab, select the only entry from the list and click on the edit button to the right.

![vbox_sharedfolders](img/vbox_sharedfolders.png)

In the popup window change the value for "Folder Path" to an existing appropriate folder on your machine.

![vbox_sharedfolders](img/vbox_sharedfolder_settings.png)

Click the "OK" button in the popup window, as well as in the "Settings" window to save the changes.

This folder will then be mounted automatically in the VM in `/mnt/shared` (or whatever you specify in "Mount point"). However, the mount point is only accessible as the `root` user inside the VM, not the regular `tutorial` user. To copy data there, use

```bash
sudo cp -r ~/bioinfo_tutorial/output /mnt/shared/
```

## Configure SSH access from host

Unfortunatelty without a desktop environment copy & paste to and from the virtual machine doesn't work. One way to solve this issue is to connect via SSH instead of using the VirtualBox window. Since VirtualBox isolates the VMs in a different subnet, we have to set up port forwarding.

First we need to know the IP of the VM:

* Start the VM and log in with the username `tutorial` and password `lcsblcsb`. If you have issues with the display size of the VM, try switching to "Scaled Mode" ("View" menu or `Cmd + c` ) and adjust the window size.

* Run `ip a s`.

   ![vbox_ip](img/vbox_ip.png)

* Look for the `enp` interface and an IP starting with `10.0.2`.

* Note down the IP you found.

* Shut down the VM again by typing `shutdown -h now`.

Then we can set up the port forwarding:

* Go again to the "Settings" of the Ubuntu VM.

* Select the "Network" tab.

* Click on the little arrow in front of "Advanced" to expand the advanced settings.

* Then click the button "Port Forwarding".

   ![vbox_network](img/vbox_network.png)
   
* In the popup window change the "Guest IP" to the IP of the VM that you noted down before.

   ![vbox_portforwarding](img/vbox_portforwarding.png)

* Click the "OK" button in the popup window, as well as in the "Settings" window to save the changes.



## Connect to the VM via SSH

* Start the VM again in VirtualBox.

* Start the `Powershell` (Windows) or other terminal application (MacOS, Linux).

* Connect to the VM with

  ```bash
  ssh -p 2222 tutorial@127.0.0.1
  ```

* Password is still `lcsblcsb`. You can change it with `passwd`, if you want.

Congratulations, you successfully configured the VirtualBox Ubuntu VM and connected to it via SSH! You can now follow the Linux instructions in the [main tutorial](../README.md).
