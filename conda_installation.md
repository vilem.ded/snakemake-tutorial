# Install conda

## Windows

You need at least Windows 10 with the [Windows 10 Fall Creator update](https://support.microsoft.com/en-gb/help/4028685/windows-10-get-the-fall-creators-update), released October 2017.

* Check that the Windows Subsytem for Linux is enabled: open "Control Panel" -> "Programs and Features" -> "Turn Windows Feature on or off"  -> check "Windows Subsystem for Linux".

* Use [this guide](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows) to install the [Ubuntu app](https://www.microsoft.com/de-de/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab) from the Microsoft Store.

* Run the Ubuntu app and change the directory to a location that you can access from Windows:

   ```bash
   $ cd /mnt/c/Users/<your_username>/
   ```

* Then follow the Linux instructions.

For older versions of Windows follow the [instructions to set up VirtualBox](virtualbox.md).

## MacOS

* Download and run the [Miniconda .pkg installer](https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.pkg) in the Python 3.7 version.

* Start the "Terminal" app or [iTerm2](https://www.iterm2.com/).

* If you use something else than `bash` as your shell, you need run

  ```bash
  $ source ~/.bash_profile
  ```

  to activate conda. You can also add the conda initialization to other shells with

  ```bash
  $ miniconda3/bin/conda init <shellname>
  ```

  e.g.

  ```bash
  $ miniconda3/bin/conda init zsh
  ```

## Linux

Start the respective terminal/console app.

```bash
$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
$ chmod u+x Miniconda3-latest-Linux-x86_64.sh
$ ./Miniconda3-latest-Linux-x86_64.sh
```
You need to specify your installation destination, e.g. `/home/<your_username>/tools/miniconda3`. You must use the **full** path and can**not** use `$HOME/tools/miniconda3`. Answer `yes` to initialize Miniconda3.

The installation will modify your `.bashrc` to make conda directly available after each login. To activate the changes now, run

```bash
$ source ~/.bashrc
```
