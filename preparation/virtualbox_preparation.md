# VirtualBox preparation

This document describes how to prepare the VirtualBox Ubuntu VM (Ubuntu.ova).

* Create new VM in VirtualBox:

  * Name: Ubuntu
  * Type: Linux
  * Version: Ubuntu (64-bit)
  * Memory Size: 1024 MB
  * Hard Disk: VDI, dynamically allocated, size: 50 GB

* Install Ubuntu **server** (since this has no desktop environment).

* During installation create a user `tutorial` with a simple password.

* After installation, do:

  ```bash
  sudo apt update
  sudo apt upgrade
  sudo apt install tree
  sudo apt install graphviz
  ```

* Install VirtualBox guest additions:
  When the VM is running, in the VirtualBox menu under "Devices" select "Insert Guest Additions CD Image..."

  ```bash
  sudo mkdir -p /media/cdrom
  sudo mount /dev/cdrom /media/cdrom
  sudo apt-get install build-essential linux-headers-`uname -r`
  cd /media/cdrom
  sudo ./VBoxLinuxAdditions.run 
  ```

* Already configure the shared folder and SSH forwarding, like described in the [user installation instructions](../virtualbox.md), and test that it's working.

* In the VirtualBox menu under "File" select "Export Appliance…" to export the VM.

* Upload the exported applicance to WebDAV.